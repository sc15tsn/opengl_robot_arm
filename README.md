Setup (Ubuntu)
---

1. Ensure that g++ and Make are installed
```sh
sudo apt install g++
sudo apt install make
```

2. Install Qt5 and qtchooser (which comes with qmake)
```sh
sudo apt install qt5-default
sudo apt install qtchooser
```

3. Ensure that the correct installation of Qt is selected
```sh
export QT_SELECT=default
```

4. Navigate to the cloned repository
5. Run qmake and make to compile the source code
```sh
qmake opengl_robot_arm.pro
make
```

6. (Optional) If running into compile errors of the kind "<GL/glu.h> no such file or directory", install the necessary OpenGL components and retry
```sh
sudo apt-get install freeglut3-dev
```

Running
---

Run the created executable to start the application
```sh
./opengl_robot_arm
```

![breakdown of the ui of the application](https://i.ibb.co/DDMckjm/opengl-robot-arm.png)

1. **Arm display**: Shows the configuration of the arm. The mouse can be used to rotate and scale the scene. The "Reset Camera" button can be used to undo those changes.
2. **Manual Movement**: The sliders can be used to adjust the angles of each joint of the arm manually. The "Reset Configuration" button can be used to revert back to the initial configuration.
3. **Auto Movement**: Used to specify a 3d point in the scene. If the point can be reached by the arm, clicking "Start" will play an animation that takes the arm from the current configuration to that point. The "Start" and "Stop" buttons can also be used to pause and resume the animation while in progress.
4. **Material Settings**: The sliders can be used to set the material properties of the arm to be used for the Phong lighting model. The sliders for the specular, diffuse, and ambient components correspond to the red, green, and blue colour channels from top to bottom. The "Reset Material" button can be used to change the colour of the arm back to black.

Demo
---

![project demo video](opengl_robot_arm.mp4)


Attribution
---

Repository icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
