#include "MainWidget.hpp"
#include <iostream>

MainWidget::MainWidget(QWidget* parent): QWidget(parent)
{

	//Create elements of widget
	armWidget = new RobotArmWidget(this);
	settings = new SettingsWidget(this);
	resetButton = new QPushButton("Reset camera");

	//Assemble layout
	mainLayout = new QHBoxLayout();
	leftLayout = new QVBoxLayout();

	leftLayout -> addWidget(armWidget);
	leftLayout -> addWidget(resetButton);

	mainLayout -> addLayout(leftLayout);
	mainLayout -> addWidget(settings);

	this -> setLayout(mainLayout);

	//Setup connections for resetting values on reset buttons clicked
	connect(resetButton, SIGNAL(clicked()), armWidget, SLOT(resetWorld()));
	connect(settings -> getMoveWidget() -> getManualMoveWidget() -> getResetButton(), SIGNAL(clicked()), armWidget, SLOT(resetLimbs()));
	connect(settings -> getMaterialSettingsWidget() -> getResetButton(), SIGNAL(clicked()), armWidget, SLOT(resetMaterial()));

	//Connections for updating limb positions slider movement
	connect(settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbOne(), SIGNAL(sliderMoved(int)), armWidget, SLOT(updateLimbOne(int)));
	connect(settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbTwo(), SIGNAL(sliderMoved(int)), armWidget, SLOT(updateLimbTwo(int)));
	connect(settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbThree(), SIGNAL(sliderMoved(int)), armWidget, SLOT(updateLimbThree(int)));
	connect(settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbFour(), SIGNAL(sliderMoved(int)), armWidget, SLOT(updateLimbFour(int)));

	//Connections for updating material properties on slider movement
	connect(settings -> getMaterialSettingsWidget() -> getRSpecular(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateRSpecular(int)));
	connect(settings -> getMaterialSettingsWidget() -> getGSpecular(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateGSpecular(int)));
	connect(settings -> getMaterialSettingsWidget() -> getBSpecular(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateBSpecular(int)));

	connect(settings -> getMaterialSettingsWidget() -> getRDiffuse(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateRDiffuse(int)));
	connect(settings -> getMaterialSettingsWidget() -> getGDiffuse(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateGDiffuse(int)));
	connect(settings -> getMaterialSettingsWidget() -> getBDiffuse(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateBDiffuse(int)));

	connect(settings -> getMaterialSettingsWidget() -> getRAmbient(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateRAmbient(int)));
	connect(settings -> getMaterialSettingsWidget() -> getGAmbient(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateGAmbient(int)));
	connect(settings -> getMaterialSettingsWidget() -> getBAmbient(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateBAmbient(int)));

	connect(settings -> getMaterialSettingsWidget() -> getShininess(), SIGNAL(valueChanged(int)), armWidget, SLOT(updateShininess(int)));

	//Connections for changing the goal position
	connect(settings -> getMoveWidget() -> getAutoMoveWidget(), SIGNAL(publishX(double)), armWidget, SLOT(setGoalX(double)));
	connect(settings -> getMoveWidget() -> getAutoMoveWidget(), SIGNAL(publishY(double)), armWidget, SLOT(setGoalY(double)));
	connect(settings -> getMoveWidget() -> getAutoMoveWidget(), SIGNAL(publishZ(double)), armWidget, SLOT(setGoalZ(double)));

	connect(settings -> getMoveWidget() -> getAutoMoveWidget() -> getStopButton(), SIGNAL(clicked()), armWidget, SLOT(resetGoalPosition()));

	//Connections for enabling/disabling manual movement when using auto movement
	connect(settings -> getMoveWidget() -> getAutoMoveWidget() -> getStartButton(), SIGNAL(clicked()), settings -> getMoveWidget() -> getManualMoveWidget(), SLOT(disable()));
	connect(settings -> getMoveWidget() -> getAutoMoveWidget() -> getStopButton(), SIGNAL(clicked()), settings -> getMoveWidget() -> getManualMoveWidget(), SLOT(enable()));

	//Connections for managing animation
	connect(settings -> getMoveWidget() -> getAutoMoveWidget() -> getStartButton(), SIGNAL(clicked()), armWidget, SLOT(startAnimation()));
	connect(armWidget, SIGNAL(animationFinished()), settings -> getMoveWidget() -> getAutoMoveWidget() -> getStopButton(), SLOT(click()));
	connect(armWidget, SIGNAL(publishLimbOneRot(int)), settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbOne(), SLOT(setValue(int)));
	connect(armWidget, SIGNAL(publishLimbTwoRot(int)), settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbTwo(), SLOT(setValue(int)));
	connect(armWidget, SIGNAL(publishLimbThreeRot(int)), settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbThree(), SLOT(setValue(int)));
	connect(armWidget, SIGNAL(publishLimbFourRot(int)), settings -> getMoveWidget() -> getManualMoveWidget() -> getLimbFour(), SLOT(setValue(int)));
	connect(settings -> getMoveWidget() -> getAutoMoveWidget() -> getStopButton(), SIGNAL(clicked()), armWidget, SLOT(forceAnimationStop()));
}

SettingsWidget* MainWidget::getSettingsWidget()
{
	return settings;
}