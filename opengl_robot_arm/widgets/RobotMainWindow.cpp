#include "RobotMainWindow.hpp"
#include "MainWidget.hpp"
#include <QSize>

RobotMainWindow::RobotMainWindow(): QMainWindow()
{
	this -> setWindowTitle("Robot Arm Simulator");
}

void RobotMainWindow::resizeEvent(QResizeEvent* event)
{
	QSize windowSize = event -> size();

	//Resize the settings widget to take up a third of the window
	MainWidget* main = (MainWidget*) this -> centralWidget();
	main -> getSettingsWidget() -> setMaximumWidth(windowSize.width() / 3);


}