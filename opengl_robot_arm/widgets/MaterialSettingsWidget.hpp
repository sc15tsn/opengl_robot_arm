#include <QFrame>
#include <QWidget>
#include <QSlider>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

class MaterialSettingsWidget: public QFrame
{

	Q_OBJECT

	public:

		MaterialSettingsWidget(QWidget* parent);

		QSlider* getRSpecular();
		QSlider* getGSpecular();
		QSlider* getBSpecular();

		QSlider* getRDiffuse();
		QSlider* getGDiffuse();
		QSlider* getBDiffuse();

		QSlider* getRAmbient();
		QSlider* getGAmbient();
		QSlider* getBAmbient();

		QSlider* getShininess();

		QPushButton* getResetButton();

	public slots:

		void resetRSpecular();
		void resetGSpecular();
		void resetBSpecular();

		void resetRDiffuse();
		void resetGDiffuse();
		void resetBDiffuse();

		void resetRAmbient();
		void resetGAmbient();
		void resetBAmbient();

		void resetShininess();

	private:

		QLabel* specularLabel;
		QLabel* diffuseLabel;
		QLabel* ambientLabel;
		QLabel* shininessLabel;

		QSlider* rSpecular;
		QSlider* gSpecular;
		QSlider* bSpecular;

		QSlider* rDiffuse;
		QSlider* gDiffuse;
		QSlider* bDiffuse;

		QSlider* rAmbient;
		QSlider* gAmbient;
		QSlider* bAmbient;

		QSlider* shininess;

		QPushButton* resetButton;

		QVBoxLayout* mainLayout;
		QHBoxLayout* topLayout;
		QHBoxLayout* bottomLayout;
		QVBoxLayout* specularLayout;
		QVBoxLayout* diffuseLayout;
		QVBoxLayout* ambientLayout;

};