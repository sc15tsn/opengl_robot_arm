#include <GL/glu.h>
#include <iostream>
#include <GL/glut.h>
#include <cmath>
#include "RobotArmWidget.hpp"
#include <QTextStream>
#include <cstddef>

#define EARTH_TEXTURE 0
#define MARC_TEXTURE 1
#define SPACE_TEXTURE 2

RobotArmWidget::RobotArmWidget(QWidget* parent): QGLWidget(parent)
{
	//Set default transformation values
	worldXRot = 0;
	worldYRot = 0;
	worldScale = 1;

	//Set default arm rotation values
	limbOneRot = 0;
	limbTwoRot = 0;
	limbThreeRot = 0;
	limbFourRot = 0;

	//Set default material for robot arm
	resetMaterial();

	mouseStartPos = *(new QPointF(0, 0));

	//Set no goal position as default
	goalX = NULL;
	goalY = NULL;
	goalZ = NULL;

	//Set default rotations as goal rotations
	goalLimbOneRot = 0;
	goalLimbTwoRot = 0;
	goalLimbThreeRot = 0;
	goalLimbFourRot = 0;

	//Set original rotations
	origLimbOneRot = 0;
	origLimbTwoRot = 0;
	origLimbThreeRot = 0;
	origLimbFourRot = 0;

	//By default, no animation is being played
	stopAnimation = true;

	solver = new PoseSolver();


	//Load images
	marc = new Image("./textures/marc.ppm");
	earth = new Image("./textures/earth.ppm");
	space = new Image("./textures/space5.png");
	floor = new Image("./textures/stone_floor.jpg");

	textureNames = new GLuint[4];

}


/* Slots for resetting camera, robot arm angles, and robot materials*/

void RobotArmWidget::resetWorld()
{
	worldXRot = 0;
	worldYRot = 0;
	worldScale = 1;
	update();
}

void RobotArmWidget::resetLimbs()
{
	limbOneRot = 0;
	limbTwoRot = 0;
	limbThreeRot = 0;
	limbFourRot = 0;
	origLimbOneRot = 0;
	origLimbTwoRot = 0;
	origLimbThreeRot = 0;
	origLimbFourRot = 0;
	update();
}

void RobotArmWidget::resetMaterial()
{
	//Reset to a silver metallic-like material
	robotMaterial.specular[0] = 0.1;
	robotMaterial.specular[1] = 0.1;
	robotMaterial.specular[2] = 0.1;
	robotMaterial.specular[3] = 1.0;

	robotMaterial.diffuse[0] = 0.16;
	robotMaterial.diffuse[1] = 0.16;
	robotMaterial.diffuse[2] = 0.16;
	robotMaterial.diffuse[3] = 1.0;

	robotMaterial.ambient[0] = 0.5;
	robotMaterial.ambient[1] = 0.5;
	robotMaterial.ambient[2] = 0.5;
	robotMaterial.ambient[3] = 1.0;

	robotMaterial.shininess = 5.0;

	update();
}


/* Methods for updating robot arm rotations */

void RobotArmWidget::updateLimbOne(int rotation)
{
	limbOneRot = rotation;
	origLimbOneRot = limbOneRot;
	update();
}

void RobotArmWidget::updateLimbTwo(int rotation)
{
	limbTwoRot = rotation;
	origLimbTwoRot = limbTwoRot;
	update();
}

void RobotArmWidget::updateLimbThree(int rotation)
{
	limbThreeRot = rotation;
	origLimbThreeRot = limbThreeRot;
	update();
}

void RobotArmWidget::updateLimbFour(int rotation)
{
	limbFourRot = rotation;
	origLimbFourRot = limbFourRot;
	update();
}


/* Methods for updating robot arm material */

void RobotArmWidget::updateRSpecular(int value)
{
	robotMaterial.specular[0] = value / 10000.0;
	update();
}

void RobotArmWidget::updateGSpecular(int value)
{
	robotMaterial.specular[1] = value / 10000.0;
	update();
}

void RobotArmWidget::updateBSpecular(int value)
{
	robotMaterial.specular[2] = value / 10000.0;
	update();
}

void RobotArmWidget::updateRDiffuse(int value)
{
	robotMaterial.diffuse[0] = value / 10000.0;
	update();
}

void RobotArmWidget::updateGDiffuse(int value)
{
	robotMaterial.diffuse[1] = value / 10000.0;
	update();
}

void RobotArmWidget::updateBDiffuse(int value)
{
	robotMaterial.diffuse[2] = value / 10000.0;
	update();
}

void RobotArmWidget::updateRAmbient(int value)
{
	robotMaterial.ambient[0] = value / 10000.0;
	update();
}

void RobotArmWidget::updateGAmbient(int value)
{
	robotMaterial.ambient[1] = value / 10000.0;
	update();
}

void RobotArmWidget::updateBAmbient(int value)
{
	robotMaterial.ambient[2] = value / 10000.0;
	update();
}

void RobotArmWidget::updateShininess(int value)
{
	robotMaterial.shininess = value;
	update();
}


/* Slots that change the goal position of the arm */

void RobotArmWidget::setGoalX(double x)
{
	goalX = x;
	update();
}

void RobotArmWidget::setGoalY(double y)
{
	goalY = y;
	update();
}

void RobotArmWidget::setGoalZ(double z)
{
	goalZ = z;
	update();
}

void RobotArmWidget::resetGoalPosition()
{
	goalX = NULL;
	goalY = NULL;
	goalZ = NULL;
	update();
}


/* Methods that set up and disable arm animation*/

void RobotArmWidget::startAnimation()
{
	//Compute the desired goal angles
	float* goalAngles = solver -> computeGoalAngles(goalX, goalY, goalZ);
	goalLimbOneRot = goalAngles[0];
	goalLimbTwoRot = goalAngles[1];
	goalLimbThreeRot = goalAngles[2];
	goalLimbFourRot = goalAngles[3];

	//Start a QTimer to make animation updates
	QTimer* timer = new QTimer();
	timer -> start(1);

	//Reset the stop boolean
	stopAnimation = false;

	//Link the timer to the animation code
	connect(timer, SIGNAL(timeout()), this, SLOT(applyAnimation()));
}

void RobotArmWidget::applyAnimation()
{

	static int rotationsApplied = 0;

	if(rotationsApplied == 100)
	{
		stopAnimation = true;
	}

	//Get the desired rotation changes
	float deltaLimbOne = (goalLimbOneRot - origLimbOneRot) / 100;
	float deltaLimbTwo = (goalLimbTwoRot - origLimbTwoRot) / 100;
	float deltaLimbThree = (goalLimbThreeRot - origLimbThreeRot) / 100;
	float deltaLimbFour = (goalLimbFourRot - origLimbFourRot) / 100;

	//Rotate each limb by that amount
	limbOneRot += deltaLimbOne;
	limbTwoRot += deltaLimbTwo;
	limbThreeRot += deltaLimbThree;
	limbFourRot += deltaLimbFour;

	//Update the sliders based on the current angles
	emit publishLimbOneRot((int)limbOneRot);
	emit publishLimbTwoRot((int)limbTwoRot);
	emit publishLimbThreeRot((int)limbThreeRot);
	emit publishLimbFourRot((int)limbFourRot);

	//If the animation should be stopped, reset all relevant values, and clean up the qtimer object
	if(stopAnimation)
	{
		emit animationFinished();
		rotationsApplied = 0;
		origLimbOneRot = limbOneRot;
		origLimbTwoRot = limbTwoRot;
		origLimbThreeRot = limbThreeRot;
		origLimbFourRot = limbFourRot;
		goalLimbOneRot = 0;
		goalLimbTwoRot = 0;
		goalLimbThreeRot = 0;
		goalLimbFourRot = 0;
		delete this -> sender();
	}

	rotationsApplied++;


	repaint();

}

void RobotArmWidget::forceAnimationStop()
{
	//Flag that the animation should be stopped
	stopAnimation = true;
}


/* Method that sets up initial OpenGL context */

void RobotArmWidget::initializeGL()
{
	//Set background colour (gray)
	glClearColor(0.5, 0.5, 0.5, 0.0);

	//Make OpenGL update normals
	glEnable(GL_NORMALIZE);

	//Set projection mode
	glMatrixMode(GL_PROJECTION);

		glLoadIdentity();
		glFrustum(-1., 1, -1, 1, 1, 100);

	//Enable depth test
	glMatrixMode(GL_MODELVIEW);

		glEnable(GL_DEPTH_TEST);

		glLoadIdentity();

		//Set up lighting
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		GLfloat light_pos[] = {-5., 5., 5, 1.};
		glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

		//Set up smmoth shading
		glShadeModel(GL_SMOOTH);

	//Set up texture options

	glEnable(GL_TEXTURE_2D);

	glGenTextures(4, textureNames);
	glBindTexture(GL_TEXTURE_2D, textureNames[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, earth -> getWidth(), earth -> getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, earth -> getBytes());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);


	glBindTexture(GL_TEXTURE_2D, textureNames[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, marc -> getWidth(), marc -> getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, marc -> getBytes());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	

	glBindTexture(GL_TEXTURE_2D, textureNames[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, space -> getWidth(), space -> getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, space -> getBytes());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, textureNames[3]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floor -> getWidth(), floor -> getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, floor -> getBytes());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glDisable(GL_TEXTURE_2D);


}


/*Method that is called when window is resized*/

void RobotArmWidget::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
}


/*Method that is called when scene is to be painted*/

void RobotArmWidget::paintGL()
{
	//Clear screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

		glLoadIdentity();

		//Setup camera
		gluLookAt(0.0, 2.0, 8.5, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

		//Apply world rotations
		glRotatef(worldXRot, 1.0, 0.0, 0.0);
		glRotatef(worldYRot, 0.0, 1.0, 0.0);

		//Apply world scaling
		glScalef(worldScale, worldScale, worldScale);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureNames[1]);

		//Draw the floor
		this -> setupFloor(4, 1, 1000);

		glDisable(GL_TEXTURE_2D);

		//Draw the robot arm
		this -> setupRobotArm();


		glEnable(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, textureNames[0]);

		//Draw a sphere around the goal point, if defined
		if(goalX != NULL && goalY != NULL && goalZ != NULL)
		{
			this -> setupGoalSphere();
		}

		glBindTexture(GL_TEXTURE_2D, textureNames[2]);
		this -> setupBackground();

		glDisable(GL_TEXTURE_2D);

	//Draw
	glFlush();
}


// Method that draws a checkerboard floor of area (size * size) at y = 0

void RobotArmWidget::setupFloor(int radius, int height, int steps)
{
	float stepSize = (2*M_PI)/steps;

	glPushMatrix();

	glRotatef(180, 0, 0, 1);

	glBindTexture(GL_TEXTURE_2D, textureNames[3]);

	for(float theta = 0; theta < 2*M_PI; theta+=stepSize)
	{
		glBegin(GL_POLYGON);

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f((theta + stepSize) / 2*M_PI, 0);
			glVertex3f(radius * cos(theta + stepSize), 0, -radius * sin(theta + stepSize));

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f((theta + stepSize/2) / 2*M_PI, 1);
			glVertex3f(0, 0, 0);

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f(theta / 2*M_PI, 0);
			glVertex3f(radius * cos(theta), 0, -radius * sin(theta));

			

		glEnd();
	}

	glBindTexture(GL_TEXTURE_2D, textureNames[1]);

	int polygonsPerTexture = steps / 20;
	int polygonsDrawn = 0;

	for(float theta = 0; theta < 2*M_PI; theta+=stepSize)
	{
		glBegin(GL_POLYGON);

			glTexCoord2f((polygonsDrawn + 1) / (float)polygonsPerTexture, 1);
			glVertex3f(radius * cos(theta + stepSize), 0, -radius * sin(theta + stepSize));

			glTexCoord2f(polygonsDrawn/ (float)polygonsPerTexture, 1);
			glVertex3f(radius * cos(theta), 0, -radius * sin(theta));

			glTexCoord2f(polygonsDrawn / (float)polygonsPerTexture, 0);
			glVertex3f(radius * cos(theta), -height, -radius * sin(theta));

			glTexCoord2f((polygonsDrawn + 1) /(float)polygonsPerTexture, 0);
			glVertex3f(radius * cos(theta + stepSize), -height, -radius * sin(theta + stepSize));

		glEnd();

		polygonsDrawn++;

		if(polygonsDrawn == polygonsPerTexture)
		{
			polygonsDrawn = 0;
		}
	}

	glBindTexture(GL_TEXTURE_2D, textureNames[3]);

	for(float theta = 0; theta < 2*M_PI; theta+=stepSize)
	{
		glBegin(GL_POLYGON);

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f((theta + stepSize) / (2*M_PI), 0);
			glVertex3f(radius * cos(theta), -height, -radius * sin(theta));

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f((theta + stepSize/2) / (2*M_PI), 1);
			glVertex3f(0, -height, 0);

			glNormal3f(0.0, 1.0, 0.0);
			glTexCoord2f(theta / (2*M_PI), 0);
			glVertex3f(radius * cos(theta + stepSize), -height, -radius * sin(theta + stepSize));

		glEnd();
	}

	glBindTexture(GL_TEXTURE_2D, textureNames[1]);

	glPopMatrix();

}


/* Method that draws the robot arm */

void RobotArmWidget::setupRobotArm()
{

	//Set material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, robotMaterial.ambient);
  	glMaterialfv(GL_FRONT, GL_DIFFUSE, robotMaterial.diffuse);
  	glMaterialfv(GL_FRONT, GL_SPECULAR, robotMaterial.specular);
 	glMaterialf(GL_FRONT, GL_SHININESS, robotMaterial.shininess);

 	//Draw the base of the robot
 	this -> setupRobotBase();

 	glPushMatrix();

 		//Apply limb one rotation and draw
 		glRotatef(limbOneRot, 0.0, 1.0, 0.0);
 		this -> setupLimbOne();

 		//Apply limb two rotation and draw
 		glTranslatef(0.0, 2.45, 0.0);
 		glRotatef(limbTwoRot, 1.0, 0.0, 0.0);
 		this -> setupLimbTwo();

 		//Apply limb three rotation and draw
 		glTranslatef(0.075, 2., 0.0);
 		glRotatef(limbThreeRot, 1.0, 0.0, 0.0);
 		this -> setupLimbThree();

 		//Apply limb four rotation and draw
 		glTranslatef(-0.075, 1., 0.0);
 		glRotatef(limbFourRot, 1.0, 0.0, 0.0);
 		this -> setupLimbFour();

 		//Draw hand
 		glTranslatef(0.0, 0.85, 0);
 		this -> setupHand();

 	glPopMatrix();
}


/* Method that draws the base that the arm is attached to */

void RobotArmWidget::setupRobotBase()
{

	glMatrixMode(GL_MODELVIEW);

		glPushMatrix();
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 1.25, 1.25, 0.4, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 0.4, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 1.25, 1.1, 0.2, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 0.6, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 1.1, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 1, 1, 1.8, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 1.8, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 1, 0.85, 0.2, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 2.0, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluDisk(gluNewQuadric(), 0.65, 0.85, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 1.9, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 0.65, 0.65, 0.1, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 1.9, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.65, 100, 10);
		glPopMatrix();
}


/* Methods for drawing cuboid shapes in OpenGL*/

void RobotArmWidget::setupCuboid(float width, float height, float breadth)
{

		//Setup normal vectors
		float normals[6][3] = {
			{0.0, 0.0, 1.0},
			{0.0, 0.0, -1.0},
			{-1.0, 0.0, 0.0},
			{1.0, 0.0, 0.0},
			{0.0, 1.0, 0.0},
			{0.0, -1.0, 0.0}};

		//Front
		glNormal3fv(normals[0]);
		glBegin(GL_POLYGON);
			glVertex3f(width / 2, height / 2, breadth / 2);
			glVertex3f(-width / 2, height / 2, breadth / 2);
			glVertex3f(-width / 2, -height / 2, breadth / 2);
			glVertex3f(width / 2, -height / 2, breadth / 2);
		glEnd();

		//Back
		glNormal3fv(normals[1]);
		glBegin(GL_POLYGON);
			glVertex3f(width / 2, height / 2, -breadth / 2);
			glVertex3f(-width / 2, height / 2, -breadth / 2);
			glVertex3f(-width / 2, -height / 2, -breadth / 2);
			glVertex3f(width / 2, -height / 2, -breadth / 2);
		glEnd();

		//Left
		glNormal3fv(normals[2]);
		glBegin(GL_POLYGON);
			glVertex3f(-width / 2, height / 2, breadth / 2);
			glVertex3f(-width / 2, height / 2, -breadth / 2);
			glVertex3f(-width / 2, -height / 2, -breadth / 2);
			glVertex3f(-width / 2, -height / 2, breadth / 2);
		glEnd();

		//Right
		glNormal3fv(normals[3]);
		glBegin(GL_POLYGON);
			glVertex3f(width / 2, height / 2, -breadth / 2);
			glVertex3f(width / 2, height / 2, breadth / 2);
			glVertex3f(width / 2, -height / 2, breadth / 2);
			glVertex3f(width / 2, -height / 2, -breadth / 2);
		glEnd();

		//Top
		glNormal3fv(normals[4]);
		glBegin(GL_POLYGON);
			glVertex3f(width / 2, height / 2, -breadth / 2);
			glVertex3f(-width / 2, height / 2, -breadth / 2);
			glVertex3f(-width / 2, height / 2, breadth / 2);
			glVertex3f(width / 2, height / 2, breadth / 2);
		glEnd();

		//Bottom
		glNormal3fv(normals[5]);
		glBegin(GL_POLYGON);
			glVertex3f(width / 2, -height / 2, -breadth / 2);
			glVertex3f(-width / 2, -height / 2, -breadth / 2);
			glVertex3f(-width / 2, -height / 2, breadth / 2);
			glVertex3f(width / 2, -height / 2, breadth / 2);
		glEnd();

}

void RobotArmWidget::setupRoundedCuboid(float width, float height, float depth)
{

	//Draw a cuboid
	glPushMatrix();
	glTranslatef(0., height/2, 0.);
	this -> setupCuboid(width, height, depth);
	glPopMatrix();

	//Add cylinders and circles to each end to round them off
	glPushMatrix();
	glTranslatef(-width/2, 0.0, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	gluCylinder(gluNewQuadric(), depth/2, depth/2, width, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(width/2, 0.0, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	gluDisk(gluNewQuadric(), 0, depth/2, 100, 10);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-width/2, 0.0, 0.0);
	glRotatef(-90, 0.0, 1.0, 0.0);
	gluDisk(gluNewQuadric(), 0, depth/2, 100, 10);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-width/2, height, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	gluCylinder(gluNewQuadric(), depth/2, depth/2, width, 100, 100);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(width/2, height, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	gluDisk(gluNewQuadric(), 0, depth/2, 100, 10);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-width/2, height, 0.0);
	glRotatef(-90, 0.0, 1.0, 0.0);
	gluDisk(gluNewQuadric(), 0, depth/2, 100, 10);
	glPopMatrix();
}


/* Methods that draw individual limbs of the robot arm */

void RobotArmWidget::setupLimbOne()
{
		glPushMatrix();
		glTranslatef(0.0, 1.9, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluCylinder(gluNewQuadric(), 0.5, 0.5, 0.05, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.0, 1.95, 0.0);
		glRotatef(-90, 1.0, 0.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.5, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.3, 2.2, 0.0);
		this -> setupCuboid(0.1, 0.5, 0.2);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.3, 2.2, 0.0);
		this -> setupCuboid(0.1, 0.5, 0.2);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.25, 2.45, 0.0);
		glRotatef(90, 0.0, 1.0, 0.0);
		gluCylinder(gluNewQuadric(), 0.1, 0.1, 0.1, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.25, 2.45, 0.0);
		glRotatef(-90, 0.0, 1.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.1, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.35, 2.45, 0.0);
		glRotatef(90, 0.0, 1.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.1, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.25, 2.45, 0.0);
		glRotatef(-90, 0.0, 1.0, 0.0);
		gluCylinder(gluNewQuadric(), 0.1, 0.1, 0.1, 100, 100);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.25, 2.45, 0.0);
		glRotatef(90, 0.0, 1.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.1, 100, 10);
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef(-0.35, 2.45, 0.0);
		glRotatef(-90, 0.0, 1.0, 0.0);
		gluDisk(gluNewQuadric(), 0, 0.1, 100, 10);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.25, 2.45, 0.0);
		glRotatef(90, 0.0, 1.0, 0.0);
		gluCylinder(gluNewQuadric(), 0.05, 0.05, 0.5, 100, 100);
		glPopMatrix();	
}	

void RobotArmWidget::setupLimbTwo()
{
	this -> setupRoundedCuboid(0.075, 2, 0.3);
}

void RobotArmWidget::setupLimbThree()
{
	this -> setupRoundedCuboid(0.075, 1, 0.3);
}

void RobotArmWidget::setupLimbFour()
{
	this -> setupRoundedCuboid(0.075, 0.7, 0.3);
}

void RobotArmWidget::setupHand()
{
	glTranslatef(0.0, 0.1, 0.0);
	this -> setupCuboid(0.05, 0.2, 0.05);

	glTranslatef(0.0, 0.1, 0.0);
	this -> setupCuboid(0.2, 0.05, 0.2);

	glPushMatrix();
	glTranslatef(-0.07, 0.125, 0.07);
	this -> setupCuboid(0.04, 0.2, 0.04);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.07, 0.125, -0.07);
	this -> setupCuboid(0.04, 0.2, 0.04);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.07, 0.125, 0.07);
	this -> setupCuboid(0.04, 0.2, 0.04);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.07, 0.125, -0.07);
	this -> setupCuboid(0.04, 0.2, 0.04);
	glPopMatrix();
}


/* Draws a sphere around the goal point*/

void RobotArmWidget::setupGoalSphere()
{

	//Draw a small sphere around the origin, and translate to the goal
	glPushMatrix();
	glTranslatef(goalX, goalY, goalZ);
	glRotatef(90, 1, 0, 0);
	glRotatef(90, 0, 0, 1);
	GLUquadric* sphere = gluNewQuadric();
	gluQuadricDrawStyle(sphere, GLU_FILL);
  	gluQuadricTexture(sphere, true);
  	gluQuadricNormals(sphere, GLU_SMOOTH);
	gluSphere(sphere, 0.35, 100, 100);
	glPopMatrix();

}

void RobotArmWidget::setupBackground()
{
	glPushMatrix();
	glTranslatef(0, -10, 0);
	glRotatef(90, 0, 0, 1);
	GLUquadric* sphere = gluNewQuadric();
	gluQuadricDrawStyle(sphere, GLU_FILL);
  	gluQuadricTexture(sphere, true);
  	gluQuadricNormals(sphere, GLU_SMOOTH);
	gluSphere(sphere, 40, 100, 100);
	glPopMatrix();
}


/* Method that records the position of the mouse when click occurs */

void RobotArmWidget::mousePressEvent(QMouseEvent* event)
{
	//Set mouse start position as the location clicked
	mouseStartPos = event -> pos();
}


/* Method that uses mouse movement to rotate the world */

void RobotArmWidget::mouseMoveEvent(QMouseEvent* event)
{

	//Get mouse movement vector from start position to current position
	QPointF mouseMovement = event -> pos() - mouseStartPos;

	//Decompose mouse movement into vertical and horizontal component
	float mouseMoveX = mouseMovement.x();
	float mouseMoveY = mouseMovement.y();

	//Use movement components to determine angle to apply, add to existing angle
	if((worldXRot + (mouseMoveY * 0.2) <= 70) && (worldXRot + (mouseMoveY * 0.2) >= 0))
	{	
		worldXRot += mouseMoveY * 0.2;
	}

	worldYRot += mouseMoveX * 0.2;

	//Wrap around if angles are greater than 360
	if(worldXRot >= 360)
	{
		worldXRot -= 360;
	}

	if(worldYRot >= 360)
	{
		worldYRot -= 360;
	}

	//Set new mouse start position as the current position
	mouseStartPos = event -> pos();

	//Repaint scene
	update();
}

void RobotArmWidget::mouseReleaseEvent(QMouseEvent* event)
{
	//Reset the mouse start position
	mouseStartPos = *(new QPointF(0, 0));
}


/* Method that uses mouse wheel movement to scale world */

void RobotArmWidget::wheelEvent(QWheelEvent* event)
{
	//Get the distance the mouse wheel was moved
	int mouseDistance = event -> delta();

	//Use this to set the scale of the world
	if(worldScale + mouseDistance * 0.0005 < 1 && worldScale + mouseDistance * 0.0005 > 0.3)
	{
		worldScale += mouseDistance * 0.0005;
	}

	//Update the scene
	update();
}