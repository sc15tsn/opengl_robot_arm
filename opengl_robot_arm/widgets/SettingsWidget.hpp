#include <QWidget>
#include <QFrame>
#include <QVBoxLayout>
#include <QWidget>
#include "MoveWidget.hpp"
#include "MaterialSettingsWidget.hpp"

class SettingsWidget: public QFrame
{

	public:

		SettingsWidget(QWidget* parent);

		MoveWidget* getMoveWidget();
		MaterialSettingsWidget* getMaterialSettingsWidget();

	private:

		MoveWidget* move;
		MaterialSettingsWidget* material;
		//LightSettingsWidget* light;

		QVBoxLayout* mainLayout;
};