#include <QGLWidget>
#include <GL/glu.h>
#include <QWidget>
#include <QMouseEvent>
#include <QPointF>
#include <QTimer>
#include "../util/PoseSolver.hpp"
#include "../util/Image.hpp"

/* Struct containing material properties */

typedef struct materialStruct
{
	float specular[4];
	float diffuse[4];
	float ambient[4];
	float shininess;

} materialStruct;


/*Class that draws a scene containing a robot arm*/

class RobotArmWidget: public QGLWidget
{

	Q_OBJECT

	public:

		RobotArmWidget(QWidget* parent);

	signals:

		void animationFinished();
		void publishLimbOneRot(int rot);
		void publishLimbTwoRot(int rot);
		void publishLimbThreeRot(int rot);
		void publishLimbFourRot(int rot);

	public slots:

		//Slots for resetting robot and camera properties
		void resetWorld();
		void resetLimbs();
		void resetMaterial();

		//Slots for changing arm angles
		void updateLimbOne(int rotation);
		void updateLimbTwo(int rotation);
		void updateLimbThree(int rotation);
		void updateLimbFour(int roation);

		//Slots for changing material
		void updateRSpecular(int value);
		void updateGSpecular(int value);
		void updateBSpecular(int value);

		void updateRDiffuse(int value);
		void updateGDiffuse(int value);
		void updateBDiffuse(int value);

		void updateRAmbient(int value);
		void updateGAmbient(int value);
		void updateBAmbient(int value);

		void updateShininess(int value);

		//Slots for changing goal position
		void setGoalX(double x);
		void setGoalY(double y);
		void setGoalZ(double z);
		void resetGoalPosition();

		//Slots for dealing with arm animation
		void startAnimation();
		void applyAnimation();
		void forceAnimationStop();

	protected:

		//OpenGL event handlers
		void initializeGL();
		void resizeGL(int width, int height);
		void paintGL();

		//Mouse event handlers
		virtual void mousePressEvent(QMouseEvent* event);
		virtual void mouseMoveEvent(QMouseEvent* event);
		virtual void mouseReleaseEvent(QMouseEvent* event);
		virtual void wheelEvent(QWheelEvent* event);

	private:

		//Rotation and scale of world
		float worldXRot;
		float worldYRot;
		float worldScale;

		//Angles of robot limbs
		float limbOneRot;
		float limbTwoRot;
		float limbThreeRot;
		float limbFourRot;

		//Material of robot
		materialStruct robotMaterial;

		//Tracks mouse position when mouse is clicked -- used for world rotations
		QPointF mouseStartPos;

		//Goal position of robot hand (-1 indicates no goal)
		double goalX;
		double goalY;
		double goalZ;

		//Arm rotations needed to reach goal position
		float goalLimbOneRot;
		float goalLimbTwoRot;
		float goalLimbThreeRot;
		float goalLimbFourRot;

		//Originial arm rotations used while animating
		float origLimbOneRot;
		float origLimbTwoRot;
		float origLimbThreeRot;
		float origLimbFourRot;

		bool stopAnimation;

		PoseSolver* solver;

		//Images for texturing
		Image* marc;
		Image* earth;
		Image* space;
		Image* floor;

		GLuint* textureNames;

		//Methods for drawing simple shapes
		void setupCuboid(float width, float height, float breadth);
		void setupRoundedCuboid(float width, float height, float depth);

		//Methods for drawing parts of the robot
		void setupRobotArm();
		void setupRobotBase();
		void setupLimbOne();
		void setupLimbTwo();
		void setupLimbThree();
		void setupLimbFour();
		void setupHand();

		//Method for drawing a sphere around the goal point
		void setupGoalSphere();
		
		//Method for drawing the floor of the world
		void setupFloor(int size, int height, int steps);
		void setupBackground();

};

