#include <QObject>
#include <QString>
#include "ManualMoveWidget.hpp"

ManualMoveWidget::ManualMoveWidget(QWidget* parent): QFrame(parent)
{
	//Create widgets
	limbOne = new QSlider(Qt::Horizontal);
	limbOne -> setMinimum(0);
	limbOne -> setMaximum(360);

	limbTwo = new QSlider(Qt::Horizontal);
	limbTwo -> setMinimum(-90);
	limbTwo -> setMaximum(90);

	limbThree = new QSlider(Qt::Horizontal);
	limbThree -> setMinimum(0);
	limbThree -> setMaximum(360);

	limbFour = new QSlider(Qt::Horizontal);
	limbFour -> setMinimum(0);
	limbFour -> setMaximum(360);

	resetButton = new QPushButton("Reset configuration");

	//Assemble widgets in layout
	mainLayout = new QVBoxLayout();

	mainLayout -> setContentsMargins(10, 70, 10, 100);
	mainLayout -> setSpacing(50);
	mainLayout -> addWidget(limbOne);
	mainLayout -> addWidget(limbTwo);
	mainLayout -> addWidget(limbThree);
	mainLayout -> addWidget(limbFour);

	mainLayout -> addWidget(resetButton);

	this -> setLayout(mainLayout);

	//Setup connections for resetting sliders on reset button clicked
	connect(this -> resetButton, SIGNAL(clicked()), this, SLOT(resetLimbOne()));
	connect(this -> resetButton, SIGNAL(clicked()), this, SLOT(resetLimbTwo()));
	connect(this -> resetButton, SIGNAL(clicked()), this, SLOT(resetLimbThree()));
	connect(this -> resetButton, SIGNAL(clicked()), this, SLOT(resetLimbFour()));
}


/* Slots for resetting slider values */

void ManualMoveWidget::resetLimbOne()
{
	limbOne -> setValue(0);
}

void ManualMoveWidget::resetLimbTwo()
{
	limbTwo -> setValue(0);
}

void ManualMoveWidget::resetLimbThree()
{
	limbThree -> setValue(0);
}

void ManualMoveWidget::resetLimbFour()
{
	limbFour -> setValue(0);
}


/* Slots for enabling/disabling this widget */

void ManualMoveWidget::enable()
{
	this -> setDisabled(false);
}

void ManualMoveWidget::disable()
{
	this -> setDisabled(true);
}


/* Methods for getting sliders */

QSlider* ManualMoveWidget::getLimbOne()
{
	return limbOne;
}

QSlider* ManualMoveWidget::getLimbTwo()
{
	return limbTwo;
}

QSlider* ManualMoveWidget::getLimbThree()
{
	return limbThree;
}

QSlider* ManualMoveWidget::getLimbFour()
{
	return limbFour;
}

QPushButton* ManualMoveWidget::getResetButton()
{
	return resetButton;
}