#include "SettingsWidget.hpp"

SettingsWidget::SettingsWidget(QWidget* parent): QFrame(parent)
{
	//Set frame style
	this -> setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

	//Create widgets
	move = new MoveWidget(this);
	material = new MaterialSettingsWidget(this);
	//light = new LightSettingsWidget(this);

	//Assemble widgets
	mainLayout = new QVBoxLayout();

	//mainLayout -> setContentsMargins(10, 0, 10, 100);
	mainLayout -> addWidget(move);
	mainLayout -> addWidget(material);
	//mainLayout -> addWidget(light);

	this -> setLayout(mainLayout);
}

MoveWidget* SettingsWidget::getMoveWidget()
{
	return move;
}

MaterialSettingsWidget* SettingsWidget::getMaterialSettingsWidget()
{
	return material;
}