#include "AutoMoveWidget.hpp"

AutoMoveWidget::AutoMoveWidget(QWidget* parent): QFrame(parent)
{
	//Create widgets
	xPos = new QDoubleSpinBox();
	xPos -> setMinimum(-5.0);
	xPos -> setMaximum(5.0);

	yPos = new QDoubleSpinBox();
	yPos -> setMinimum(1.0);
	yPos -> setMaximum(5.0);

	zPos = new QDoubleSpinBox();
	zPos -> setMinimum(-5.0);
	zPos -> setMaximum(5.0);

	xLabel = new QLabel("Goal X:");
	yLabel = new QLabel("Goal Y:");
	zLabel = new QLabel("Goal Z:");

	startButton = new QPushButton("Start");
	stopButton = new QPushButton("Stop");

	//Layout widgets
	mainLayout = new QGridLayout();

	mainLayout -> addWidget(xLabel, 0, 0);
	mainLayout -> addWidget(yLabel, 1, 0);
	mainLayout -> addWidget(zLabel, 2, 0);

	mainLayout -> addWidget(xPos, 0, 1);
	mainLayout -> addWidget(yPos, 1, 1);
	mainLayout -> addWidget(zPos, 2, 1);

	mainLayout -> addWidget(startButton, 3, 0);
	mainLayout -> addWidget(stopButton, 3, 1);

	this -> setLayout(mainLayout);

	//Set widget to emit position when start button is clicked
	connect(this -> startButton, SIGNAL(clicked()), this, SLOT(publishGoalPosition()));

	//Make start button enable/disable on clicking
	connect(this -> startButton, SIGNAL(clicked()), this, SLOT(disableInteraction()));
	connect(this -> stopButton, SIGNAL(clicked()), this, SLOT(enableInteraction()));

	//Validate spinbox input before publishing
	connect(this -> xPos, SIGNAL(editingFinished()), this, SLOT(validateSpinbox()));
	connect(this -> zPos, SIGNAL(editingFinished()), this, SLOT(validateSpinbox()));
}


/* Getters for the buttons */

QPushButton* AutoMoveWidget::getStartButton()
{
	return startButton;
}

QPushButton* AutoMoveWidget::getStopButton()
{
	return stopButton;
}


/* Slot that publishes goal values */

void AutoMoveWidget::publishGoalPosition()
{	
	//Emit position values
	emit this -> publishX(xPos -> value());
	emit this -> publishY(yPos -> value());
	emit this -> publishZ(zPos -> value());
}


/* Slots for setting widget interaction */

void AutoMoveWidget::disableInteraction()
{
	startButton -> setDisabled(true);
	xPos -> setDisabled(true);
	yPos -> setDisabled(true);
	zPos -> setDisabled(true);
}

void AutoMoveWidget::enableInteraction()
{
	startButton -> setDisabled(false);
	xPos -> setDisabled(false);
	yPos -> setDisabled(false);
	zPos -> setDisabled(false);
}


/* Force spinbox values to be less than -1 or greater than 1 */
void AutoMoveWidget::validateSpinbox()
{

	QDoubleSpinBox* box = (QDoubleSpinBox*) this -> sender();

	if(box -> value() <= -0.0 and box -> value() > -1.0)
	{
		box -> setValue(-1);
	}

	else if(box -> value() >= 0.0 and box -> value() < 1.0)
	{
		box -> setValue(1);
	}
}