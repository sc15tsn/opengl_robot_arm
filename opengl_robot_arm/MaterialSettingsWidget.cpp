#include "MaterialSettingsWidget.hpp"

MaterialSettingsWidget::MaterialSettingsWidget(QWidget* parent): QFrame(parent)
{	
	//Set QFrame style
	this -> setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	//Create widget elements
	specularLabel = new QLabel("Specular:");
	diffuseLabel = new QLabel("Diffuse:");
	ambientLabel = new QLabel("Ambient:");
	shininessLabel = new QLabel("Shininess:");

	rSpecular = new QSlider(Qt::Horizontal);
	rSpecular -> setMinimum(0);
	rSpecular -> setMaximum(10000);

	gSpecular = new QSlider(Qt::Horizontal);
	gSpecular -> setMinimum(0);
	gSpecular -> setMaximum(10000);

	bSpecular = new QSlider(Qt::Horizontal);
	bSpecular -> setMinimum(0);
	bSpecular -> setMaximum(10000);

	rDiffuse = new QSlider(Qt::Horizontal);
	rDiffuse -> setMinimum(0);
	rDiffuse -> setMaximum(10000);

	gDiffuse = new QSlider(Qt::Horizontal);
	gDiffuse -> setMinimum(0);
	gDiffuse -> setMaximum(10000);

	bDiffuse = new QSlider(Qt::Horizontal);
	bDiffuse -> setMinimum(0);
	bDiffuse -> setMaximum(10000);

	rAmbient = new QSlider(Qt::Horizontal);
	rAmbient -> setMinimum(0);
	rAmbient -> setMaximum(10000);

	gAmbient = new QSlider(Qt::Horizontal);
	gAmbient -> setMinimum(0);
	gAmbient -> setMaximum(10000);

	bAmbient = new QSlider(Qt::Horizontal);
	bAmbient -> setMinimum(0);
	bAmbient -> setMaximum(10000);

	shininess = new QSlider(Qt::Horizontal);
	shininess -> setMinimum(0);
	shininess -> setMaximum(128);

	resetButton = new QPushButton("Reset material");

	//Arrange in layouts
	mainLayout = new QVBoxLayout();
	topLayout = new QHBoxLayout();
	specularLayout = new QVBoxLayout();
	diffuseLayout = new QVBoxLayout();
	ambientLayout = new QVBoxLayout();

	specularLayout -> addWidget(specularLabel);
	specularLayout -> addWidget(rSpecular);
	specularLayout -> addWidget(gSpecular);
	specularLayout -> addWidget(bSpecular);

	diffuseLayout -> addWidget(diffuseLabel);
	diffuseLayout -> addWidget(rDiffuse);
	diffuseLayout -> addWidget(gDiffuse);
	diffuseLayout -> addWidget(bDiffuse);

	ambientLayout -> addWidget(ambientLabel);
	ambientLayout -> addWidget(rAmbient);
	ambientLayout -> addWidget(gAmbient);
	ambientLayout -> addWidget(bAmbient);

	topLayout -> addLayout(specularLayout);
	topLayout -> addLayout(diffuseLayout);
	topLayout -> addLayout(ambientLayout);

	mainLayout -> addLayout(topLayout);
	mainLayout -> addWidget(shininessLabel);
	mainLayout -> addWidget(shininess);
	mainLayout -> addWidget(resetButton);

	this -> setLayout(mainLayout);

	//Setup connections for resetting slider positions
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetRSpecular()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetGSpecular()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetBSpecular()));

	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetRDiffuse()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetGDiffuse()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetBDiffuse()));

	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetRAmbient()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetGAmbient()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetBAmbient()));

	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetShininess()));

	//Reset sliders on creation
	resetRSpecular();
	resetGSpecular();
	resetBSpecular();

	resetRDiffuse();
	resetGDiffuse();
	resetBDiffuse();

	resetRAmbient();
	resetGAmbient();
	resetBAmbient();

	resetShininess();

}

/* Slots for resetting material property sliders*/

void MaterialSettingsWidget::resetRSpecular()
{
	rSpecular -> setValue(1000);
}

void MaterialSettingsWidget::resetGSpecular()
{
	gSpecular -> setValue(1000);
}

void MaterialSettingsWidget::resetBSpecular()
{
	bSpecular -> setValue(1000);
}

void MaterialSettingsWidget::resetRDiffuse()
{
	rDiffuse -> setValue(1600);
}

void MaterialSettingsWidget::resetGDiffuse()
{
	gDiffuse -> setValue(1600);
}

void MaterialSettingsWidget::resetBDiffuse()
{
	bDiffuse -> setValue(1600);
}

void MaterialSettingsWidget::resetRAmbient()
{
	rAmbient -> setValue(5000);
}

void MaterialSettingsWidget::resetGAmbient()
{
	gAmbient -> setValue(5000);
}

void MaterialSettingsWidget::resetBAmbient()
{
	bAmbient -> setValue(5000);
}

void MaterialSettingsWidget::resetShininess()
{
	shininess -> setValue(5);
}


/* Methods for getting components of the widget */

QSlider* MaterialSettingsWidget::getRSpecular()
{
	return rSpecular;
}

QSlider* MaterialSettingsWidget::getGSpecular()
{
	return gSpecular;
}

QSlider* MaterialSettingsWidget::getBSpecular()
{
	return bSpecular;
}

QSlider* MaterialSettingsWidget::getRDiffuse()
{
	return rDiffuse;
}

QSlider* MaterialSettingsWidget::getGDiffuse()
{
	return gDiffuse;
}

QSlider* MaterialSettingsWidget::getBDiffuse()
{
	return bDiffuse;
}

QSlider* MaterialSettingsWidget::getRAmbient()
{
	return rAmbient;
}

QSlider* MaterialSettingsWidget::getGAmbient()
{
	return gAmbient;
}

QSlider* MaterialSettingsWidget::getBAmbient()
{
	return bAmbient;
}

QSlider* MaterialSettingsWidget::getShininess()
{
	return shininess;
}

QPushButton* MaterialSettingsWidget::getResetButton()
{
	return resetButton;
}