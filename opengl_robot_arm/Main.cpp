#include <GL/glut.h>
#include "widgets/MainWidget.hpp"
#include "widgets/RobotMainWindow.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>

int main(int argc, char** argv)
{
	//Create app and fullscreen window
	QApplication app(argc, argv);
	glutInit(&argc, argv);

	RobotMainWindow window;

	//Attach opengl widget to window
	MainWidget* mainWidget = new MainWidget(NULL);
	window.setCentralWidget(mainWidget);

	//Display window and enter event loop
	window.showMaximized();
	return app.exec();
}