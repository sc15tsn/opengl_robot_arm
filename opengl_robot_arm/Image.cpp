#include "Image.hpp"
#include <QImage>
#include <QColor>
#include <QString>
#include <iostream>

Image::Image(const char* filename)
{

	//Use Qt to load in image from file
	QImage* img = new QImage(QString(filename));

	height = img -> height();
	width = img -> width();

	//Use image data to populate glubyte array
	imageBytes = new GLubyte[width * height * 3];

	for(int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int pixelPos = 3*i*width + 3*j;
			QRgb pixelColour = img -> pixel(j, i);
			imageBytes[pixelPos] = qRed(pixelColour);
			imageBytes[pixelPos + 1] = qGreen(pixelColour);
			imageBytes[pixelPos + 2] = qBlue(pixelColour);
		}
	}

}


GLubyte* Image::getBytes()
{
	return imageBytes;
}

int Image::getHeight()
{
	return height;
}

int Image::getWidth()
{
	return width;
}