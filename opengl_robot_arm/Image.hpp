#include <GL/glu.h>
#include <string>

class Image
{

	public:
		Image(const char* filename);
		GLubyte* getBytes();
		int getHeight();
		int getWidth();

	private:
		GLubyte* imageBytes;
		int height;
		int width;


};