#include <QFrame>
#include <QLabel>
#include <QWidget>
#include <QSpinBox>
#include <QPushButton>
#include <QGridLayout>
#include <QObject>

class AutoMoveWidget: public QFrame
{

	Q_OBJECT

	public:

		AutoMoveWidget(QWidget* parent);

		QPushButton* getStartButton();
		QPushButton* getStopButton();

	public slots:

		//Slot that listens for button click, and emits goal position
		void publishGoalPosition();

		//Slots for setting widget interaction
		void disableInteraction();
		void enableInteraction();

		//Slot that forces spinbox values to be < -1 or > 1
		void validateSpinbox();


	signals:

		//Signals for publishing the goal position
		void publishX(double x);
		void publishY(double y);
		void publishZ(double z);

	private:
		
		QLabel* xLabel;
		QLabel* yLabel;
		QLabel* zLabel;

		QDoubleSpinBox* xPos;
		QDoubleSpinBox* yPos;
		QDoubleSpinBox* zPos;

		QPushButton* startButton;
		QPushButton* stopButton;

		QGridLayout* mainLayout;



};