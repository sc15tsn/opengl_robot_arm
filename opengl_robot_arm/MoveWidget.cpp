#include "MoveWidget.hpp"

MoveWidget::MoveWidget(QWidget* parent): QFrame(parent)
{
	//Setup widgets
	manualWidget = new ManualMoveWidget(this);
	autoWidget = new AutoMoveWidget(this);
	QFrame* separator = new QFrame();
	separator -> setFrameStyle(QFrame::VLine | QFrame::Sunken);

	//Lay widgets out
	mainLayout = new QHBoxLayout();

	mainLayout -> addWidget(manualWidget);
	mainLayout -> addWidget(separator);
	mainLayout -> addWidget(autoWidget);

	this -> setLayout(mainLayout);

	//Set frame style of this widget
	this -> setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
}


/* Methods for getting child widgets */

ManualMoveWidget* MoveWidget::getManualMoveWidget()
{
	return manualWidget;
}

AutoMoveWidget* MoveWidget::getAutoMoveWidget()
{
	return autoWidget;
}