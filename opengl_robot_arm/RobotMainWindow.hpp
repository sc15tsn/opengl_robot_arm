#include <QMainWindow>
#include <QResizeEvent>

class RobotMainWindow: public QMainWindow
{

	public:

		RobotMainWindow();

	protected:

		virtual void resizeEvent(QResizeEvent* event);
};