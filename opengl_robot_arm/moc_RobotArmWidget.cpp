/****************************************************************************
** Meta object code from reading C++ file 'RobotArmWidget.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "RobotArmWidget.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'RobotArmWidget.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_RobotArmWidget_t {
    QByteArrayData data[38];
    char stringdata0[485];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RobotArmWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RobotArmWidget_t qt_meta_stringdata_RobotArmWidget = {
    {
QT_MOC_LITERAL(0, 0, 14), // "RobotArmWidget"
QT_MOC_LITERAL(1, 15, 17), // "animationFinished"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 17), // "publishLimbOneRot"
QT_MOC_LITERAL(4, 52, 3), // "rot"
QT_MOC_LITERAL(5, 56, 17), // "publishLimbTwoRot"
QT_MOC_LITERAL(6, 74, 19), // "publishLimbThreeRot"
QT_MOC_LITERAL(7, 94, 18), // "publishLimbFourRot"
QT_MOC_LITERAL(8, 113, 10), // "resetWorld"
QT_MOC_LITERAL(9, 124, 10), // "resetLimbs"
QT_MOC_LITERAL(10, 135, 13), // "resetMaterial"
QT_MOC_LITERAL(11, 149, 13), // "updateLimbOne"
QT_MOC_LITERAL(12, 163, 8), // "rotation"
QT_MOC_LITERAL(13, 172, 13), // "updateLimbTwo"
QT_MOC_LITERAL(14, 186, 15), // "updateLimbThree"
QT_MOC_LITERAL(15, 202, 14), // "updateLimbFour"
QT_MOC_LITERAL(16, 217, 7), // "roation"
QT_MOC_LITERAL(17, 225, 15), // "updateRSpecular"
QT_MOC_LITERAL(18, 241, 5), // "value"
QT_MOC_LITERAL(19, 247, 15), // "updateGSpecular"
QT_MOC_LITERAL(20, 263, 15), // "updateBSpecular"
QT_MOC_LITERAL(21, 279, 14), // "updateRDiffuse"
QT_MOC_LITERAL(22, 294, 14), // "updateGDiffuse"
QT_MOC_LITERAL(23, 309, 14), // "updateBDiffuse"
QT_MOC_LITERAL(24, 324, 14), // "updateRAmbient"
QT_MOC_LITERAL(25, 339, 14), // "updateGAmbient"
QT_MOC_LITERAL(26, 354, 14), // "updateBAmbient"
QT_MOC_LITERAL(27, 369, 15), // "updateShininess"
QT_MOC_LITERAL(28, 385, 8), // "setGoalX"
QT_MOC_LITERAL(29, 394, 1), // "x"
QT_MOC_LITERAL(30, 396, 8), // "setGoalY"
QT_MOC_LITERAL(31, 405, 1), // "y"
QT_MOC_LITERAL(32, 407, 8), // "setGoalZ"
QT_MOC_LITERAL(33, 416, 1), // "z"
QT_MOC_LITERAL(34, 418, 17), // "resetGoalPosition"
QT_MOC_LITERAL(35, 436, 14), // "startAnimation"
QT_MOC_LITERAL(36, 451, 14), // "applyAnimation"
QT_MOC_LITERAL(37, 466, 18) // "forceAnimationStop"

    },
    "RobotArmWidget\0animationFinished\0\0"
    "publishLimbOneRot\0rot\0publishLimbTwoRot\0"
    "publishLimbThreeRot\0publishLimbFourRot\0"
    "resetWorld\0resetLimbs\0resetMaterial\0"
    "updateLimbOne\0rotation\0updateLimbTwo\0"
    "updateLimbThree\0updateLimbFour\0roation\0"
    "updateRSpecular\0value\0updateGSpecular\0"
    "updateBSpecular\0updateRDiffuse\0"
    "updateGDiffuse\0updateBDiffuse\0"
    "updateRAmbient\0updateGAmbient\0"
    "updateBAmbient\0updateShininess\0setGoalX\0"
    "x\0setGoalY\0y\0setGoalZ\0z\0resetGoalPosition\0"
    "startAnimation\0applyAnimation\0"
    "forceAnimationStop"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RobotArmWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      29,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  159,    2, 0x06 /* Public */,
       3,    1,  160,    2, 0x06 /* Public */,
       5,    1,  163,    2, 0x06 /* Public */,
       6,    1,  166,    2, 0x06 /* Public */,
       7,    1,  169,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,  172,    2, 0x0a /* Public */,
       9,    0,  173,    2, 0x0a /* Public */,
      10,    0,  174,    2, 0x0a /* Public */,
      11,    1,  175,    2, 0x0a /* Public */,
      13,    1,  178,    2, 0x0a /* Public */,
      14,    1,  181,    2, 0x0a /* Public */,
      15,    1,  184,    2, 0x0a /* Public */,
      17,    1,  187,    2, 0x0a /* Public */,
      19,    1,  190,    2, 0x0a /* Public */,
      20,    1,  193,    2, 0x0a /* Public */,
      21,    1,  196,    2, 0x0a /* Public */,
      22,    1,  199,    2, 0x0a /* Public */,
      23,    1,  202,    2, 0x0a /* Public */,
      24,    1,  205,    2, 0x0a /* Public */,
      25,    1,  208,    2, 0x0a /* Public */,
      26,    1,  211,    2, 0x0a /* Public */,
      27,    1,  214,    2, 0x0a /* Public */,
      28,    1,  217,    2, 0x0a /* Public */,
      30,    1,  220,    2, 0x0a /* Public */,
      32,    1,  223,    2, 0x0a /* Public */,
      34,    0,  226,    2, 0x0a /* Public */,
      35,    0,  227,    2, 0x0a /* Public */,
      36,    0,  228,    2, 0x0a /* Public */,
      37,    0,  229,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Double,   29,
    QMetaType::Void, QMetaType::Double,   31,
    QMetaType::Void, QMetaType::Double,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void RobotArmWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RobotArmWidget *_t = static_cast<RobotArmWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->animationFinished(); break;
        case 1: _t->publishLimbOneRot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->publishLimbTwoRot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->publishLimbThreeRot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->publishLimbFourRot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->resetWorld(); break;
        case 6: _t->resetLimbs(); break;
        case 7: _t->resetMaterial(); break;
        case 8: _t->updateLimbOne((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->updateLimbTwo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->updateLimbThree((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->updateLimbFour((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->updateRSpecular((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->updateGSpecular((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->updateBSpecular((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->updateRDiffuse((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->updateGDiffuse((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->updateBDiffuse((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->updateRAmbient((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->updateGAmbient((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->updateBAmbient((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->updateShininess((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->setGoalX((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 23: _t->setGoalY((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->setGoalZ((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 25: _t->resetGoalPosition(); break;
        case 26: _t->startAnimation(); break;
        case 27: _t->applyAnimation(); break;
        case 28: _t->forceAnimationStop(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (RobotArmWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RobotArmWidget::animationFinished)) {
                *result = 0;
            }
        }
        {
            typedef void (RobotArmWidget::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RobotArmWidget::publishLimbOneRot)) {
                *result = 1;
            }
        }
        {
            typedef void (RobotArmWidget::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RobotArmWidget::publishLimbTwoRot)) {
                *result = 2;
            }
        }
        {
            typedef void (RobotArmWidget::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RobotArmWidget::publishLimbThreeRot)) {
                *result = 3;
            }
        }
        {
            typedef void (RobotArmWidget::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RobotArmWidget::publishLimbFourRot)) {
                *result = 4;
            }
        }
    }
}

const QMetaObject RobotArmWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_RobotArmWidget.data,
      qt_meta_data_RobotArmWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *RobotArmWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RobotArmWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_RobotArmWidget.stringdata0))
        return static_cast<void*>(const_cast< RobotArmWidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int RobotArmWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 29)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 29;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 29)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 29;
    }
    return _id;
}

// SIGNAL 0
void RobotArmWidget::animationFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void RobotArmWidget::publishLimbOneRot(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void RobotArmWidget::publishLimbTwoRot(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void RobotArmWidget::publishLimbThreeRot(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void RobotArmWidget::publishLimbFourRot(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
