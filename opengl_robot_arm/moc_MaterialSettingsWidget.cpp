/****************************************************************************
** Meta object code from reading C++ file 'MaterialSettingsWidget.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MaterialSettingsWidget.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MaterialSettingsWidget.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MaterialSettingsWidget_t {
    QByteArrayData data[12];
    char stringdata0[168];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MaterialSettingsWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MaterialSettingsWidget_t qt_meta_stringdata_MaterialSettingsWidget = {
    {
QT_MOC_LITERAL(0, 0, 22), // "MaterialSettingsWidget"
QT_MOC_LITERAL(1, 23, 14), // "resetRSpecular"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 14), // "resetGSpecular"
QT_MOC_LITERAL(4, 54, 14), // "resetBSpecular"
QT_MOC_LITERAL(5, 69, 13), // "resetRDiffuse"
QT_MOC_LITERAL(6, 83, 13), // "resetGDiffuse"
QT_MOC_LITERAL(7, 97, 13), // "resetBDiffuse"
QT_MOC_LITERAL(8, 111, 13), // "resetRAmbient"
QT_MOC_LITERAL(9, 125, 13), // "resetGAmbient"
QT_MOC_LITERAL(10, 139, 13), // "resetBAmbient"
QT_MOC_LITERAL(11, 153, 14) // "resetShininess"

    },
    "MaterialSettingsWidget\0resetRSpecular\0"
    "\0resetGSpecular\0resetBSpecular\0"
    "resetRDiffuse\0resetGDiffuse\0resetBDiffuse\0"
    "resetRAmbient\0resetGAmbient\0resetBAmbient\0"
    "resetShininess"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MaterialSettingsWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    0,   65,    2, 0x0a /* Public */,
       4,    0,   66,    2, 0x0a /* Public */,
       5,    0,   67,    2, 0x0a /* Public */,
       6,    0,   68,    2, 0x0a /* Public */,
       7,    0,   69,    2, 0x0a /* Public */,
       8,    0,   70,    2, 0x0a /* Public */,
       9,    0,   71,    2, 0x0a /* Public */,
      10,    0,   72,    2, 0x0a /* Public */,
      11,    0,   73,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MaterialSettingsWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MaterialSettingsWidget *_t = static_cast<MaterialSettingsWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetRSpecular(); break;
        case 1: _t->resetGSpecular(); break;
        case 2: _t->resetBSpecular(); break;
        case 3: _t->resetRDiffuse(); break;
        case 4: _t->resetGDiffuse(); break;
        case 5: _t->resetBDiffuse(); break;
        case 6: _t->resetRAmbient(); break;
        case 7: _t->resetGAmbient(); break;
        case 8: _t->resetBAmbient(); break;
        case 9: _t->resetShininess(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MaterialSettingsWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_MaterialSettingsWidget.data,
      qt_meta_data_MaterialSettingsWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MaterialSettingsWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MaterialSettingsWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MaterialSettingsWidget.stringdata0))
        return static_cast<void*>(const_cast< MaterialSettingsWidget*>(this));
    return QFrame::qt_metacast(_clname);
}

int MaterialSettingsWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
