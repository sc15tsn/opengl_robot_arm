#include <QFrame>
#include <QWidget>
#include <QSlider>
#include <QPushButton>
#include <QVBoxLayout>
#include <QObject>

/* A class that allows the user to manually control the robot's limb angles */

class ManualMoveWidget: public QFrame
{
	Q_OBJECT

	public:

		ManualMoveWidget(QWidget* parent);

		QSlider* getLimbOne();
		QSlider* getLimbTwo();
		QSlider* getLimbThree();
		QSlider* getLimbFour();
		QPushButton* getResetButton();

	public slots:

		//Slots for resetting sliders
		void resetLimbOne();
		void resetLimbTwo();
		void resetLimbThree();
		void resetLimbFour();

		//Slots for enabling/disabling the widget
		void enable();
		void disable();
		
	private:

		//Sliders for setting limb angles
		QSlider* limbOne;
		QSlider* limbTwo;
		QSlider* limbThree;
		QSlider* limbFour;
		
		//Button for resetting angles
		QPushButton* resetButton;

		QVBoxLayout* mainLayout;
};