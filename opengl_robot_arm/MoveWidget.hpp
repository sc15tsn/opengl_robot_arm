#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include "ManualMoveWidget.hpp"
#include "AutoMoveWidget.hpp"

class MoveWidget: public QFrame
{

	public:

		MoveWidget(QWidget* parent);

		ManualMoveWidget* getManualMoveWidget();
		AutoMoveWidget* getAutoMoveWidget();
		
	private:

		ManualMoveWidget* manualWidget;
		AutoMoveWidget* autoWidget;

		QHBoxLayout* mainLayout;
};