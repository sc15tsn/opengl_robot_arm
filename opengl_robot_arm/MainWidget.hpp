#include "RobotArmWidget.hpp"
#include "SettingsWidget.hpp"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>

class MainWidget: public QWidget
{
	public:

		MainWidget(QWidget* parent);
		SettingsWidget* getSettingsWidget();

	private:
		RobotArmWidget* armWidget;
		SettingsWidget* settings;
		QPushButton* resetButton;

		QHBoxLayout* mainLayout;
		QVBoxLayout* leftLayout;
		

};