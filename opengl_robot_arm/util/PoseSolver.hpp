class PoseSolver
{

	public:

		PoseSolver();
		float* computeGoalAngles(float goalX, float goalY, float goalZ);

	private:
		float* findCircleIntersection(float circle1[4], float circle2[4], float normal[3]);
		float dot(float vec1[3], float vec2[3]);
		float len(float vec[3]);

};