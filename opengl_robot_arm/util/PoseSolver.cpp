#include "PoseSolver.hpp"
#include <cmath>
#include <cstddef>

PoseSolver::PoseSolver()
{

}


/* Find the angles the limbs should be at to reach a desired goal pose */

float* PoseSolver::computeGoalAngles(float goalX, float goalY, float goalZ)
{
		
	float unitZ[3] = {0, 0, -1};
	float goal[3] = {goalX, 0, goalZ};

	float* goalAngles = new float[4];

	//Find the angle between the x-z components of the goal position, and the z axis
	float rotOne = (180 / M_PI) * acos(dot(goal, unitZ) / len(goal));

	if(goalX > 0)
	{
		rotOne = 360 - rotOne;
	}

	//Iterate over all possible limb two rotations
	float rotTwo = 90;

	while(rotTwo > -90)
	{

		//Place the end of the hand at the goal and find the circle the 4th joint would trace
		float goalCircle[4];
		goalCircle[0] = goalX;
		goalCircle[1] = goalY;
		goalCircle[2] = goalZ;
		goalCircle[3] = 1.2;

		//Place the start of the 3rd joint at the end of the 2nd joint and find the circle it would trace
		float limbThreeCircle[4];
		limbThreeCircle[0] = 2 * sin((M_PI / 180) * rotTwo) * sin((M_PI / 180) * rotOne);
		limbThreeCircle[1] = (2 * cos((M_PI / 180) * rotTwo)) + 2.45;
		limbThreeCircle[2] = (2 * sin((M_PI / 180) * rotTwo)) * cos((M_PI / 180) * rotOne);
		limbThreeCircle[3] = 1;

		//Find the vector normal to the plane these to circles lie on i.e. the x axis in limb 2's coordinate frame
		float normal[3];
		normal[0] = cos((M_PI / 180) * rotOne);
		normal[1] = 0;
		normal[2] = -sin((M_PI / 180) * rotOne);

		//If these circles intersect, the goal can be reached, so find the angles corresponding to this intersection
		float* intersection = findCircleIntersection(goalCircle, limbThreeCircle, normal);
		
		//If there was no solution, move on to the next angle of limb 2
		if(intersection == NULL)
		{
			rotTwo-=0.1;
			continue;
		}

		else
		{
			
			goalAngles[0] = rotOne;
			goalAngles[1] = rotTwo;

			//Work out vector pointing along limb 2
			float limbTwoVector[3];
			limbTwoVector[0] = limbThreeCircle[0];
			limbTwoVector[1] = limbThreeCircle[1] - 2.45;
			limbTwoVector[2] = limbThreeCircle[2];

			//Subtract from intersection to get desired limb3 vector
			float desiredLimbThreeVector[3];
			desiredLimbThreeVector[0] = intersection[0] - limbThreeCircle[0];
			desiredLimbThreeVector[1] = intersection[1] - limbThreeCircle[1];
			desiredLimbThreeVector[2] = intersection[2] - limbThreeCircle[2];

			//Dot vectors to get angle
			float rotThree = (180 / M_PI) * acos(dot(desiredLimbThreeVector, limbTwoVector) / (len(desiredLimbThreeVector) * len(limbTwoVector)));
			goalAngles[2] = 360 - rotThree;

			goalAngles[3] = 0;

		}

		break;
	}

	return goalAngles;

}


/*Finds the intersection of two circles, circle1 and circle2, lying along the plane defined by the circle centers and normal*/

float* PoseSolver::findCircleIntersection(float circle1[4], float circle2[4], float normal[3])
{

	//Calculate the difference between the centers of the two circles
	float difference[3];
	difference[0] = circle2[0] - circle1[0];
	difference[1] = circle2[1] - circle1[1];
	difference[2] = circle2[2] - circle1[2];

	//If the distance is larger than the sum of the radii, then the circles do not overlap, so there is no solution
	if(len(difference) > circle1[3] + circle2[3])
	{
		return NULL;
	}

	//Use the difference and normal vectors to find the 3rd normal vector lying in the same plane as difference, and calculate its length too
	float otherNormal[3];
	otherNormal[0] = normal[1]*difference[2] - normal[2]*difference[1];
	otherNormal[1] = normal[2]*difference[0] - normal[0]*difference[2];
	otherNormal[2] = normal[0]*difference[1] - normal[1]*difference[0];

	//Find the distance along the difference and normal vectors to the intersection
	float parallelDistance = (pow(len(difference), 2) - pow(circle2[3], 2) + pow(circle1[3], 2)) / (2.0 * len(difference));
	float perpendicularDistance = sqrt(pow(circle1[3], 2) - pow(parallelDistance, 2));

	//Normalize and scale the difference and normal vectors by these amounts
	float parallelVector[3];
	float perpendicularVector[3];
		
	for(int i = 0; i < 3; i++)
	{
		parallelVector[i] = (difference[i]/len(difference)) * parallelDistance;
		perpendicularVector[i] = (otherNormal[i]/len(otherNormal)) * perpendicularDistance;
	}

	//Add these vectors to circle one's center to get the intersection point
	float* intersection = new float[3];
	
	for(int i = 0; i < 3; i++)
	{
		intersection[i] = circle1[i] + parallelVector[i] + perpendicularVector[i]; 
	}

	return intersection;

}


/* Vector operations */

float PoseSolver::dot(float vec1[3], float vec2[3])
{
	return (vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2]);
}

float PoseSolver::len(float vec[3])
{
	return sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
}

